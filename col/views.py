from django.shortcuts import render
import pyowm
from django.templatetags.static import static

# Create your views here.

def index(request):
    """View function for home page of site."""
    
    owm = pyowm.OWM('7d76d0022ff718cad6ef2cfcebc44719')
    current_loc = owm.weather_at_place('Heathrow,uk')
    current_obs = current_loc.get_weather()
    current_wind = current_obs.get_wind()
    
    # Dummy value
    wind_dir = current_wind['deg']
    wind_speed = current_wind['speed']
    speed_factor = 9 * (10 / wind_speed)
    this_dir_idx = int(wind_dir / 10)
    this_dir_loc = static('/col/data/velocity_low_res/magU_'
    	+ str(this_dir_idx) + '.vtp')
    this_dir_stream = static('col/data/streams/' 
    	+ str(int(wind_dir)) + '_deg.vtp')

    context = {
        'wind_dir': wind_dir,
        'wind_speed': wind_speed,
        'this_dir_loc': this_dir_loc,
        'this_speed_factor': speed_factor,
        'this_dir_stream': this_dir_stream,
    }

    return render(request, 'col/index.html', context=context)

def about(request):
	context={}
	return render(request, 'col/about.html', context=context)
